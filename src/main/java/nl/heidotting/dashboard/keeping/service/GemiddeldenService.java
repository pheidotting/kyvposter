package nl.heidotting.dashboard.keeping.service;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Map;

@Service
public class GemiddeldenService {
    public Double berekenUrenGemiddeldNogNodig(Map<String, Object> uren, LocalDate vandaag) {
        Double urenNogTeGaan = getUrenNotTeGaan(uren, vandaag);// 36-   (Double)    uren.get("aantalUren");

        Double urenGemiddeldNodig = urenNogTeGaan / Double.valueOf(Integer.valueOf(5 - vandaag.getDayOfWeek().getValue() + 1).toString());

        Double urenVandaag = (Double) uren.get("aantalUren" + vandaag.getDayOfWeek().getValue());
        urenVandaag = urenVandaag + (Double) uren.get("aantalUren");
        if (urenVandaag > urenGemiddeldNodig) {
            urenNogTeGaan = 36.0;
            for (int i = 1; i <= 5; i++) {
                urenNogTeGaan = urenNogTeGaan - (Double) uren.get("aantalUren" + i);
            }

            urenGemiddeldNodig = urenNogTeGaan / Double.valueOf(Integer.valueOf(5 - vandaag.getDayOfWeek().getValue()).toString());
        }

        return urenGemiddeldNodig < 0 ? 0 : urenGemiddeldNodig;
    }

    private Double getUrenNotTeGaan(Map<String, Object> uren, LocalDate vandaag) {
        Double urenNogTeGaan = 36.0;
        for (int i = 1; i <= 5; i++) {
            urenNogTeGaan = urenNogTeGaan - (Double) uren.get("aantalUren" + i);
        }

        return urenNogTeGaan - (Double) uren.get("aantalUren");
    }
}
