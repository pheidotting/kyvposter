package nl.heidotting.service;

import io.prometheus.client.Gauge;
import nl.heidotting.dashboard.keeping.service.MetricsService;
import org.brunocvcunha.instagram4j.requests.payload.InstagramComment;
import org.brunocvcunha.instagram4j.requests.payload.InstagramFeedItem;
import org.brunocvcunha.instagram4j.requests.payload.InstagramUserSummary;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;

public class InstagramBotService extends Thread {
    private final static Logger LOGGER = LoggerFactory.getLogger(InstagramBotService.class);

    private MetricsService metricsService;
    private InstagramServiceNw instagramService;

    private Gauge instagramFollowersGauge;
    private Gauge instagramFollowingGauge;
    private Gauge instagramPostsGauge;
    private Gauge instagramLikesGauge;
    private Gauge instagramCommentsGauge;

    public InstagramBotService(MetricsService metricsService, InstagramServiceNw instagramService, Gauge instagramFollowersGauge, Gauge instagramFollowingGauge, Gauge instagramPostsGauge, Gauge instagramLikesGauge, Gauge instagramCommentsGauge) {
        this.metricsService = metricsService;
        this.instagramService = instagramService;
        this.instagramFollowersGauge = instagramFollowersGauge;
        this.instagramFollowingGauge = instagramFollowingGauge;
        this.instagramPostsGauge = instagramPostsGauge;
        this.instagramLikesGauge = instagramLikesGauge;
        this.instagramCommentsGauge = instagramCommentsGauge;
    }

    @Override
    public void run() {
        ophalenGegevensVoorInstagram();
    }

    public void ophalenGegevensVoorInstagram() {
        List<String> teVolgenTags = newArrayList(//
                "oldtimerdagklazinaveen", //
                "oldtimerdagklazienaveen", //
                "klveneryoungtimervrienden", //
                "klazienaveneryoungtimervrienden", //
                "klazinaveneryoungtimervrienden", //
                "youngtimervrienden", //
                "youngtimervriendenklazienaveen",//
                "youngtimervriendenklazinaveen"//
        );

        Map<String, List<String>> teVolgenDubbeleTags = newHashMap();
        teVolgenDubbeleTags.put("oldtimerdag$", newArrayList("klazienaveen"));
        teVolgenDubbeleTags.put("oldtimerdag#", newArrayList("klazinaveen"));
        teVolgenDubbeleTags.put("youngtimerdag$", newArrayList("klazienaveen"));
        teVolgenDubbeleTags.put("youngtimerdag#", newArrayList("klazinaveen"));
        teVolgenDubbeleTags.put("oldtimer$", newArrayList("klazienaveen"));
        teVolgenDubbeleTags.put("oldtimer#", newArrayList("klazinaveen"));
        teVolgenDubbeleTags.put("youngtimer$", newArrayList("klazienaveen"));
        teVolgenDubbeleTags.put("youngtimer#", newArrayList("klazinaveen"));
        teVolgenTags.stream().forEach(s -> teVolgenDubbeleTags.put(s, null));

        List<String> teLikenTags = newArrayList(//
                "classiccar",//
                "classiccars",//
                "classiccarspotting",//
                "historiccar",//
                "historiccars",//
                "oldcar",//
                "oldcars",//
                "oldschoolcar",//
                "oldschoolcars",//
                "oldtimer", //
                "oldtimerdag", //
                "oldtimergarage",//
                "oldtimerlove",//
                "oldtimermeeting", //
                "oldtimers", //
                "oldtimertreffen", //
                "retrocar",//
                "retrocars",//
                "youngtimer",//
                "youngtimerdag", //
                "youngtimergarage",//
                "youngtimerlove",//
                "youngtimermeeting",//
                "youngtimers",//
                "youngtimertreffen"//
        );

        LOGGER.info("Kijk in de eigen feed wie posts liken en commenten en volg deze");
        instagramService.getFeed().stream().filter(instagramFeedItem -> instagramFeedItem != null)//
                .forEach(instagramFeedItem -> {
                    if (instagramFeedItem.getLike_count() > 0 && instagramFeedItem.getLikers() != null && !instagramFeedItem.getLikers().isEmpty()) {
                        instagramFeedItem.getLikers().stream().forEach(volgAlleUsersDieFotosLiken());
                    }
                    if (instagramFeedItem.getComment_count() > 0 && instagramFeedItem.getComments() != null && !instagramFeedItem.getComments().isEmpty()) {
                        instagramFeedItem.getComments().forEach(volgAlleUsersDieCommentAchterlieten());
                    }
                });
        instagramService.resetFollowersEnFollowing();

        LOGGER.info("Dubbele tags zoeken om te volgen");
        teVolgenDubbeleTags.keySet().stream().forEach(//
                tagT -> {
                    String tag = tagT.replace("$", "").replace("#", "");
                    LOGGER.debug("Tag {} zoeken", tag);
                    instagramService.zoekOpTag(tag, (Predicate<InstagramFeedItem>) instagramFeedItem -> !instagramService.isEigen(instagramFeedItem), new Consumer<InstagramFeedItem>() {
                        @Override
                        public void accept(InstagramFeedItem instagramFeedItem) {
                            LOGGER.trace("{}", instagramFeedItem.getPk());
                            if (!instagramService.isAlGeliked(instagramFeedItem) && instagramFeedItem.getUser().getPk() != 4917904352L) {
                                if (teVolgenDubbeleTags.get(tagT) == null) {
                                    likeEnVolg(instagramFeedItem);
                                } else {
                                    if (teVolgenDubbeleTags.get(tagT).stream().allMatch(s -> instagramFeedItem.getCaption() != null && instagramFeedItem.getCaption().getText() != null && instagramFeedItem.getCaption().getText().contains("#" + s))) {
                                        likeEnVolg(instagramFeedItem);
                                    }
                                }
                            }
                        }
                    });
                    LOGGER.trace("next");
                });

        LOGGER.info("Tags zoeken om te liken");
        List<InstagramFeedItem> gevondenTags = teLikenTags.stream().map(tag -> {
            LOGGER.debug("tag {}", tag);
            return instagramService.zoekOpTag(tag, isNietNullNietEigenEnNietAlGelikedFeedItem(), null).collect(Collectors.toList());
        }).flatMap(Collection::stream).collect(Collectors.toList());

        List<InstagramFeedItem> shuffled = gevondenTags.stream().filter(instagramFeedItem -> !instagramFeedItem.isHas_liked()).collect(toShuffledList());//

        List<InstagramFeedItem> vijftig = shuffled.subList(0, 50);

        vijftig.stream()//
                .forEach(likeFeedItems());

        LOGGER.info("Users die ons volgen, terugvolgen");
        instagramService.getInstagramFollowers().stream().forEach(instagramUserSummary -> instagramService.volgUserIndienNietAlGevolgd(instagramUserSummary, false));

        LOGGER.info("Klaar met users die ons volgen, terugvolgen");

        new Thread(new InstagramStatistiekenService(metricsService, instagramService, instagramFollowersGauge, instagramFollowingGauge, instagramPostsGauge, instagramLikesGauge, instagramCommentsGauge, true)).run();
    }

    private Consumer<InstagramUserSummary> volgAlleUsersDieFotosLiken() {
        return instagramUserSummary -> instagramService.volgUserIndienNietAlGevolgd(instagramUserSummary, false);
    }

    private Consumer<Object> volgAlleUsersDieCommentAchterlieten() {
        return instagramComment -> instagramService.volgUserIndienNietAlGevolgd(((InstagramComment) instagramComment).getUser(), false);
    }

    private Predicate<InstagramFeedItem> isNietNullNietEigenEnNietAlGelikedFeedItem() {
        return instagramFeedItem -> instagramFeedItem != null && !instagramService.isEigen(instagramFeedItem) && !instagramService.isAlGeliked(instagramFeedItem);
    }

    private Consumer<InstagramFeedItem> likeFeedItems() {
        return instagramFeedItem -> {
            instagramService.like(instagramFeedItem);
        };
    }

    private static final Collector<?, ?, ?> SHUFFLER = Collectors.collectingAndThen(Collectors.toCollection(ArrayList::new), list -> {
        Collections.shuffle(list);
        return list;
    });

    @SuppressWarnings("unchecked")
    public static <T> Collector<T, ?, List<T>> toShuffledList() {
        return (Collector<T, ?, List<T>>) SHUFFLER;
    }

    private void likeEnVolg(InstagramFeedItem instagramFeedItem) {
        if (instagramService.like(instagramFeedItem)) {
            instagramService.volgUserIndienNietAlGevolgd(instagramFeedItem.getUser(), true);

            LOGGER.info("Versturen direct message");
            String tekst = instagramFeedItem.getUser().getFull_name() + " (" + instagramFeedItem.getUser().getUsername() + ")";
            String url = null;
            try {
                url = instagramFeedItem.getImage_versions2().getCandidates().get(0).getUrl();
            } catch (Exception e) {
            }
            instagramService.sendDirectMessage(tekst, url, "3431928379");
        }
    }
}
