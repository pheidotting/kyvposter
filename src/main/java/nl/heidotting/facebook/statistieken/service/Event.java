package nl.heidotting.facebook.statistieken.service;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Event {
    @JsonProperty("attending_count")
    private int attending_count;
    @JsonProperty("declined_count")
    private int declined_count;
    @JsonProperty("maybe_count")
    private int maybe_count;
    @JsonProperty("id")
    private String id;
    @JsonProperty("start_time")
    private String start_time;
    @JsonProperty("name")
    private String name;

    public Event() {
    }

    public Event(int attending_count, int declined_count, int maybe_count, String id, String start_time) {
        this.attending_count = attending_count;
        this.declined_count = declined_count;
        this.maybe_count = maybe_count;
        this.id = id;
        this.start_time = start_time;
    }

    public int getAttending_count() {
        return attending_count;
    }

    public void setAttending_count(int attending_count) {
        this.attending_count = attending_count;
    }

    public int getDeclined_count() {
        return declined_count;
    }

    public void setDeclined_count(int declined_count) {
        this.declined_count = declined_count;
    }

    public int getMaybe_count() {
        return maybe_count;
    }

    public void setMaybe_count(int maybe_count) {
        this.maybe_count = maybe_count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}