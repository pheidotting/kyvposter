package nl.lakedigital.djfc.service;

import com.google.common.util.concurrent.RateLimiter;
import nl.lakedigital.djfc.domain.virtueleoldtimerdag.Bestand;

import java.io.File;
import java.util.function.Consumer;

import static com.google.common.collect.Lists.newArrayList;

public class FacebookAlbumVirtueleOldtimerdagService {
    private String BASE_DIR = "/Users/patrickheidotting/Pictures/Virtuele Oldtimerdag 2020";

    private final RateLimiter rateLimiter = RateLimiter.create(1.0);

    public void start() {
        FacebookService facebookService = new FacebookService();

        File dir = new File(BASE_DIR);
        newArrayList(dir.listFiles()).stream().//
                //                filter(file -> file.getName().toLowerCase().startsWith("92811052_1569312866551826_1322714253446086656_")).//
                        filter(file -> file.getName().toLowerCase().endsWith(".jpg") || file.getName().toLowerCase().endsWith(".jepg")).//
                map(new PlateRecognitionService().recognize(rateLimiter)).//
                map(new RdwService().leesHashTagsMetWaade()).//
                forEach((Consumer<Bestand>) s -> facebookService.maakAlbum(s));
    }


}
