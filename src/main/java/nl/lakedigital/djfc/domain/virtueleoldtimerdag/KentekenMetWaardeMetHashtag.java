package nl.lakedigital.djfc.domain.virtueleoldtimerdag;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class KentekenMetWaardeMetHashtag {
    private String kenteken;
    private List<WaardeMetHashtag> waardeMetHashtags = newArrayList();

    public KentekenMetWaardeMetHashtag() {
    }

    public KentekenMetWaardeMetHashtag(String kenteken) {
        this.kenteken = kenteken;
    }

    public KentekenMetWaardeMetHashtag(String kenteken, List<WaardeMetHashtag> waardeMetHashtags) {
        this.kenteken = kenteken;
        this.waardeMetHashtags = waardeMetHashtags;
    }

    public String getKenteken() {
        return kenteken;
    }

    public void setKenteken(String kenteken) {
        this.kenteken = kenteken;
    }

    public List<WaardeMetHashtag> getWaardeMetHashtags() {
        return waardeMetHashtags;
    }

    public void setWaardeMetHashtags(List<WaardeMetHashtag> waardeMetHashtags) {
        this.waardeMetHashtags = waardeMetHashtags;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(null).append("kenteken", kenteken).append("waardeMetHashtags", waardeMetHashtags).toString();
    }
}
