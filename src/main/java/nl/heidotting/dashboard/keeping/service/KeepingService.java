package nl.heidotting.dashboard.keeping.service;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONException;
import kong.unirest.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class KeepingService {
    private final static Logger LOGGER = LoggerFactory.getLogger(KeepingService.class);

    private String TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjQ3MzliMGUyMGVhY2FlOGVhMWFmYTQyNGFlYmM4Zjk2Yzc4ODA4M2QwZjdlZmNiMGEyYmEyZjgxYmVhM2Q3NjUyYjA0Yjk0MjdlYThkM2ZiIn0.eyJhdWQiOiIzIiwianRpIjoiNDczOWIwZTIwZWFjYWU4ZWExYWZhNDI0YWViYzhmOTZjNzg4MDgzZDBmN2VmY2IwYTJiYTJmODFiZWEzZDc2NTJiMDRiOTQyN2VhOGQzZmIiLCJpYXQiOjE1ODcxNDYwMzksIm5iZiI6MTU4NzE0NjAzOSwiZXhwIjoxNjE4NjgyMDM4LCJzdWIiOiI3MDg0Iiwic2NvcGVzIjpbInRpbWUiLCJyZXBvcnRpbmciLCJwcm9qZWN0X21hbmFnZW1lbnQiLCJ0ZWFtIl19.F_JcWNk13C2fmrdKvJEon2gnuqbZL1H3-P2NS9gVDbuV8ZGTLeAsQe27PMjCm03V98Hv0t3yQ4-0hUgxCmnAO-lS700yoshX1P6Di_E6NCTg-01njT82yAFz3CldHlWVQgUKxU2nnfwPv_TT5wlxwkl-C3vKQEEUoWEQQ0zaxu4HS03T4BxUllkUJ0qbou912uOX1B6ezyxz17hROhtTvKCRXWCXUVST0ktmSFOEyjVlh1PXVLmjQgE-bQJ4ujxprg9Jai1G8YHu6x61bBFpUYIjY_8hA0vQ6mFGFCKA5rgAV90igvCoGGclpIyOqAD-CV1y4kPMAXJTb7fDPOREczR8pwdeXZ1I-ihm8M4hci5LdliD91YBQEohatBfq-G4Jalxh-xAcpo6OrjD2-6SVqHqwXfYio-FuUwQi0dvAjlmPPqGuCSWMEtGN9-SZebeuX9LcwMoKdItfiU_CSVQqYZ5AG1Ao15KeQeaW5STs9hDjpYuOCYUTgYi65B8gqXXJhA3Lo1Qj7rtqUx72qhG4f34IMs1LAd0NWtjdNyfjaSfk4828EZFfC9nhYrXbyf7SEcXKuiBodQ7l96dlROss878YuX8M51XbYV1HFrMvNNygHaF7GR8JsB0GA2wuSwDsST2Vh4wjUMlhCDr8FwVFmRZ6pdpjg7pmTptMnXE3uc";
    private String URL_ORGANISATIONS = "https://api.keeping.nl/v1/organisations";
    private String URL_TIME_ENTRIES = "https://api.keeping.nl/v1/{organisation_id}/time-entries";
    private String URL_REPORT = "https://api.keeping.nl/v1/{organisation_id}/report";

    public Map<String, Object> leesTotaalAantalUren(List<LocalDate> dagen) {
        Map<String, Object> result = new HashMap<>();

        HttpResponse<JsonNode> response = Unirest.get(URL_ORGANISATIONS).header("Authorization", "Bearer " + TOKEN).asJson();

        JSONObject node = response.getBody().getObject();

        JSONArray array = node.getJSONArray("organisations");
        JSONObject oranisation = (JSONObject) array.get(0);
        String oranisationId = oranisation.getString("id");

        Double aantalUren = 0.0;

        HttpResponse<JsonNode> responseEntries = Unirest.get(URL_REPORT.replace("{organisation_id}", oranisationId)).queryString("from", dagen.get(0).toString()).queryString("to", dagen.get(dagen.size() - 1).toString()).queryString("row_type", "week").header("Authorization", "Bearer " + TOKEN).asJson();

        JSONObject report = responseEntries.getBody().getObject().getJSONObject("report");
        JSONArray reportArray = report.getJSONArray("rows");
        for (int i = 0; i < reportArray.length(); i++) {
            JSONObject entry = (JSONObject) reportArray.get(i);

            LOGGER.debug("Aantal uren : {}", entry.getDouble("hours"));

            aantalUren = aantalUren + entry.getDouble("hours");
        }

        Double urenVandaag = urenVoorEenDag(oranisationId, LocalDate.now());

        aantalUren = aantalUren + urenVandaag;
        LOGGER.debug("Aantal uren : {}", aantalUren);

        LOGGER.debug("Ongoing {}", urenVandaag != 0.0);
        result.put("ongoing", urenVandaag != 0.0);
        result.put("aantalUren", aantalUren);

        for (LocalDate dag : dagen) {
            HttpResponse<JsonNode> responseDag = Unirest.get(URL_REPORT.replace("{organisation_id}", oranisationId)).queryString("from", dag.toString()).queryString("to", dag.toString()).queryString("row_type", "day").header("Authorization", "Bearer " + TOKEN).asJson();

            JSONObject reportDag = responseDag.getBody().getObject().getJSONObject("report");
            JSONArray reportDagArray = reportDag.getJSONArray("rows");
            Double aantalUrenDag = 0.0;
            for (int i = 0; i < reportDagArray.length(); i++) {
                JSONObject entry = (JSONObject) reportDagArray.get(i);

                LOGGER.debug("Aantal uren voor {} : {}", dag.getDayOfWeek().getValue(), entry.getDouble("hours"));

                aantalUrenDag = aantalUrenDag + entry.getDouble("hours");
            }
                aantalUrenDag = aantalUrenDag + urenVoorEenDag(oranisationId, dag);

            result.put("aantalUren" + dag.getDayOfWeek().getValue(), aantalUrenDag);
        }

        return result;
    }


    private Double urenVoorEenDag(String oranisationId, LocalDate dag) {
        HttpResponse<JsonNode> responseEntries = Unirest.get(URL_TIME_ENTRIES.replace("{organisation_id}", oranisationId)).queryString("date", dag.toString()).header("Authorization", "Bearer " + TOKEN).asJson();

        JSONObject node = responseEntries.getBody().getObject();
        JSONArray array = null;
        try {
            array = node.getJSONArray("time_entries");
        } catch (JSONException e) {

        }
        if (array != null) {
            for (int i = 0; i < array.length(); i++) {
                JSONObject entry = (JSONObject) array.get(i);
                if (entry.getBoolean("ongoing")) {
                    LocalDateTime ldt = LocalDateTime.parse(entry.getString("start").substring(0, 19));
                    Duration duration = Duration.between(ldt, LocalDateTime.now());
                    Double d = new Double((duration.getSeconds() / 60));
                    return d / 60;

                }
            }
        }

        return 0.0;
    }
}
