package nl.lakedigital.djfc.service;

import com.google.common.util.concurrent.RateLimiter;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import nl.lakedigital.djfc.domain.virtueleoldtimerdag.Bestand;

import java.io.File;
import java.util.List;
import java.util.function.Function;

import static com.google.common.collect.Lists.newArrayList;

public class PlateRecognitionService {
    private String token = "d301c4715074e4d7a57cb6126e44f2119aec6e3f";
    private boolean recursive = false;

    public Function recognize(RateLimiter rateLimiter) {
        return (Function<File, Bestand>) file -> {
            rateLimiter.acquire();

            return voeruit(file, rateLimiter);
        };
    }

    private Bestand voeruit(File file, RateLimiter rateLimiter) {
        List<String> plates = newArrayList();
        //                    plates.add("hj375p");
        //                    plates.add("h490bn");

        try {
            HttpResponse<JsonNode> response = Unirest.post("http://api.platerecognizer.com/v1/plate-reader/").header("Authorization", "Token " + token).field("upload", file).asJson();
            //                                System.out.println("Recognize:");
            //                                System.out.println(response.getBody().toString());

            JSONObject node = response.getBody().getObject();

            JSONArray array = node.getJSONArray("results");

            for (int i = 0; i < array.length(); i++) {
                JSONObject result = (JSONObject) array.get(i);

                JSONObject region = result.getJSONObject("region");
                String regionCode = region.getString("code");

                if ("nl".equalsIgnoreCase(regionCode)) {
                    plates.add(result.getString("plate"));
                }
            }


        } catch (Exception e) {
            if (!recursive) {
                recursive = true;
                rateLimiter.acquire();
                return voeruit(file, rateLimiter);
            }
            //                            System.out.println(e);
            //                            e.printStackTrace();
        }

        //                            System.out.println(plates);
        return new Bestand(file, plates, false);
    }
}
