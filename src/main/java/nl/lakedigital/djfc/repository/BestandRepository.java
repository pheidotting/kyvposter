package nl.lakedigital.djfc.repository;

import nl.lakedigital.djfc.domain.Bestand;
import org.hibernate.*;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Repository
public class BestandRepository {
    private final static Logger LOGGER = LoggerFactory.getLogger(BestandRepository.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Session getSession() {
        try {
            return sessionFactory.getCurrentSession();
        } catch (HibernateException e) {//NOSONAR
            return sessionFactory.openSession();
        }
    }

    protected Session getEm() {
        return sessionFactory.getCurrentSession();
    }

    protected Transaction getTransaction() {
        Transaction transaction = getSession().getTransaction();
        if (transaction.getStatus() != TransactionStatus.ACTIVE) {
            transaction.begin();
        }

        return transaction;
    }

    
    public void opslaan(Bestand bestand) {
        opslaan(newArrayList(bestand));
    }


    public void opslaan(List<Bestand> bestanden) {
        getTransaction();
        for (Bestand bestand : bestanden) {
            if (bestand.getId() == null) {
                getSession().save(bestand);
            } else {
                getSession().merge(bestand);
            }
        }
        getTransaction().commit();
    }


    public List<Bestand> leesRandomAantal(int aantal) {
        Query query = getSession().getNamedQuery("Bestand.leesRandomAantal").setMaxResults(aantal);
        //            query.setParameter("aantal",aantal);

        return query.list();
    }
}
