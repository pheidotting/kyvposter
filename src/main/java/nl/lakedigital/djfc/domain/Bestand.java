package nl.lakedigital.djfc.domain;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "BESTANDEN")
@NamedQueries({@NamedQuery(name = "Bestand.leesRandomAantal", query = "select b from Bestand b ORDER BY RAND()")
        //        @NamedQuery(name = "IngeplandePost.ingeplandeOnverzondenPostsVoorDatum", query = "select p from IngeplandePost p where p.tijdstipUitgevoerd is null and p.tijdstipIngepland > :startTijdstip"),// and p.tijdstipIngepland < :eindTijdstip")
        //        @NamedQuery(name = "IngeplandePost.overgeblevenPosts", query = "select p from IngeplandePost p where p.tijdstipUitgevoerd is null"),// and p.tijdstipIngepland < :eindTijdstip")
        //        @NamedQuery(name = "IngeplandePost.leesBijResource", query = "select p from IngeplandePost p where p.resource = :resource"),//
        //        @NamedQuery(name = "IngeplandePost.laatstVerstuurdePost", query = "select p from IngeplandePost p order by p.tijdstipUitgevoerd desc"),//
        //        @NamedQuery(name = "IngeplandePost.opruimen", query = "delete from IngeplandePost p where p.tijdstipUitgevoerd < :tijdstip")
})
public class Bestand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "IDENTIFICATIE")
    private String identificatie;
    @Column(name = "BESTANDSNAAM")
    private String bestandsnaam;
    @Column(name = "MEDIA")
    private Media media;
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "bestand")
    private IngeplandePost ingeplandePost;

    public Bestand() {
        setIdentificatie(UUID.randomUUID().toString() + ".jpg");
    }

    public Bestand(Media media, String bestandsnaam) {
        this();
        this.media = media;
        this.bestandsnaam = bestandsnaam.replace("/remote.php/webdav/", "");
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentificatie() {
        return identificatie;
    }

    public void setIdentificatie(String identificatie) {
        this.identificatie = identificatie;
    }

    public String getBestandsnaam() {
        return bestandsnaam;
    }

    public void setBestandsnaam(String bestandsnaam) {
        this.bestandsnaam = bestandsnaam;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public IngeplandePost getIngeplandePost() {
        return ingeplandePost;
    }

    public void setIngeplandePost(IngeplandePost ingeplandePost) {
        this.ingeplandePost = ingeplandePost;
    }
}
