package nl.lakedigital.djfc.service;

import com.google.common.util.concurrent.RateLimiter;
import nl.lakedigital.djfc.domain.Bestand;
import nl.lakedigital.djfc.domain.IngeplandePost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class PostInplannenEnUitvoerenService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PostInplannenEnUitvoerenService.class);

    @Inject
    private IngeplandePostService ingeplandePostService;
    @Inject
    private PostInplanService postInplanService;
    @Inject
    private UitvoerenService uitvoerenService;
    @Inject
    private TagService tagService;
    @Inject
    private StackStorageService stackStorageService;
    @Inject
    private BestandService bestandService;

    private RateLimiter rateLimiter = RateLimiter.create(1);

    public void planEnVoerUit() {
        List<IngeplandePost> alleIngeplandePostsVandaag = ingeplandePostService.ingeplandePostsVoorDatum(LocalDate.now());
        List<IngeplandePost> alleIngeplandeOnverzondenPostsVandaag = ingeplandePostService.ingeplandeOnverzondenPostsVoorDatum(LocalDate.now());
        List<IngeplandePost> overgeblevenPosts = ingeplandePostService.overgeblevenPosts();

        if (alleIngeplandePostsVandaag.isEmpty()) {
            PostInplanService.Dag dag = PostInplanService.Dag.getFromDayOfWeek();
            overgeblevenPosts.stream().forEach(ingeplandePost -> {
                ingeplandePost.setTijdstipIngepland(LocalDateTime.of(LocalDate.now(), dag.getStartTijd().plusMinutes(10)));
                ingeplandePost.setOpgepakt(false);
            });
            ingeplandePostService.opslaan(overgeblevenPosts);

            List<LocalTime> tijdstippen = postInplanService.planPosts(LocalDate.now(), overgeblevenPosts.size());
            List<Bestand> bestanden = bestandService.leesRandom(tijdstippen.size());

            AtomicInteger teller = new AtomicInteger();
            List<IngeplandePost> nieuweLijst = tijdstippen.stream().map(geplandePost -> {
                //                LOGGER.info("{} - {}", geplandePost.getBestand().getMedia(), geplandePost.getTijdstip());
                IngeplandePost ingeplandePost = new IngeplandePost();

                //                        new StackFile(tagService.genereerTags(davResource.toString(), WEBDAV_PATH), davResource.toString());
                //
                //
                //                ingeplandePost.setResource(geplandePost.getStackFile().getUrl());
                ingeplandePost.setTijdstipIngepland(LocalDateTime.of(LocalDate.now(), geplandePost));
                //                ingeplandePost.setMedia(geplandePost.getBestand().getMedia());
                ingeplandePost.setBestand(bestanden.get(teller.getAndIncrement()));

                return ingeplandePost;
            }).collect(Collectors.toList());

            ingeplandePostService.opslaan(nieuweLijst);
        }

        alleIngeplandeOnverzondenPostsVandaag = ingeplandePostService.ingeplandeOnverzondenPostsVoorDatum(LocalDate.now());

        alleIngeplandeOnverzondenPostsVandaag.stream().filter(ingeplandePost -> ingeplandePost.getTijdstipIngepland().isBefore(LocalDateTime.now())).forEach(ingeplandePost -> {
            if (!ingeplandePostService.isOpgepakt(ingeplandePost)) {
                ingeplandePostService.pakOp(ingeplandePost);

                LOGGER.trace("net voor rateLimiter.acquire();");
                rateLimiter.acquire();
                LOGGER.trace("net na rateLimiter.acquire();");

                //                StackFile stackFile = new StackFile(tagService.genereerTags(ingeplandePost.getBestand().getBestandsnaam(), stackStorageService.getWEBDAV_PATH()), ingeplandePost.getBestand().getBestandsnaam());

                //                GeplandePost geplandePost = new GeplandePost(ingeplandePost.getId(), ingeplandePost.getBestand(), ingeplandePost.getTijdstipIngepland());
                //
                //                LOGGER.info("Uitvoeren post met id {}, media is {}", ingeplandePost.getId(), ingeplandePost.getBestand().getMedia());

                PostInplanService.Dag vandaag = PostInplanService.Dag.getFromDayOfWeek();
                LocalDateTime nu = LocalDateTime.now();
                LocalDateTime start = LocalDateTime.of(LocalDate.now(), vandaag.getStartTijd());
                LocalDateTime eind = LocalDateTime.of(LocalDate.now(), vandaag.getEindTijd());
                if (nu.isAfter(start) && nu.isBefore(eind)) {
                    try {
                        uitvoerenService.voeruit(ingeplandePost, alleIngeplandePostsVandaag.size());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                stackStorageService.opruimen(ingeplandePost);
            }
        });
    }

}
