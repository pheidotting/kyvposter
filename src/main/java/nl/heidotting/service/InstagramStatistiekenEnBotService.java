package nl.heidotting.service;

import io.prometheus.client.Gauge;
import nl.heidotting.dashboard.keeping.service.MetricsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.io.IOException;

@Service
public class InstagramStatistiekenEnBotService {
    private final static Logger LOGGER = LoggerFactory.getLogger(InstagramStatistiekenEnBotService.class);

    @Inject
    private InstagramServiceNw instagramService;
    @Inject
    private MetricsService metricsService;

    private Gauge instagramFollowersGauge;
    private Gauge instagramFollowingGauge;
    private Gauge instagramPostsGauge;
    private Gauge instagramLikesGauge;
    private Gauge instagramCommentsGauge;

    @PostConstruct
    public void init() throws IOException {
        if (instagramFollowersGauge == null) {
            instagramFollowersGauge = metricsService.getGauge(this.getClass(), "klazienavenerYoungtimerVriendenInstagramFollowersGauge");
        }
        if (instagramFollowingGauge == null) {
            instagramFollowingGauge = metricsService.getGauge(this.getClass(), "klazienavenerYoungtimerVriendenInstagramFollowingGauge");
        }
        if (instagramPostsGauge == null) {
            instagramPostsGauge = metricsService.getGauge(this.getClass(), "klazienavenerYoungtimerVriendenInstagramPostsGauge");
        }
        if (instagramLikesGauge == null) {
            instagramLikesGauge = metricsService.getGauge(this.getClass(), "klazienavenerYoungtimerVriendenInstagramLikesGauge");
        }
        if (instagramCommentsGauge == null) {
            instagramCommentsGauge = metricsService.getGauge(this.getClass(), "klazienavenerYoungtimerVriendenInstagramCommentsGauge");
        }
    }


    public void trapAf() {
        LOGGER.info("Aftrappen");

        instagramService.reset();
        LOGGER.info("Instagram Feed opgehaald, {} items", instagramService.getFeed().size());

        (new InstagramBotService(metricsService, instagramService, instagramFollowersGauge, instagramFollowingGauge, instagramPostsGauge, instagramLikesGauge, instagramCommentsGauge)).start();
        (new InstagramStatistiekenService(metricsService, instagramService, instagramFollowersGauge, instagramFollowingGauge, instagramPostsGauge, instagramLikesGauge, instagramCommentsGauge, false)).start();
    }
}
