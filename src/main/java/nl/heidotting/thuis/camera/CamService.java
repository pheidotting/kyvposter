package nl.heidotting.thuis.camera;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class CamService {
    private final static Logger LOGGER = LoggerFactory.getLogger(CamService.class);

    private List<Camera> urls = newArrayList();

    @PostConstruct
    public void init() {
        //urls.add(new Camera(1, "http://192.168.1.100:88/cgi-bin/CGIProxy.fcgi?cmd=snapPicture2&usr=patrick&pwd=Herman79!"));
        urls.add(new Camera(1, "http://31.21.148.75:99/cgi-bin/CGIProxy.fcgi?cmd=snapPicture2&usr=patrick&pwd=Herman79!"));
    }

    public List<Camera> getUrls() {
        return urls;
    }

    public byte[] downloadImage(Camera camera) {
        ByteArrayOutputStream out = null;
        try {
            URL url = new URL(camera.getUrl());
            InputStream in = new BufferedInputStream(url.openStream());
            out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n = 0;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return out.toByteArray();
    }
}
