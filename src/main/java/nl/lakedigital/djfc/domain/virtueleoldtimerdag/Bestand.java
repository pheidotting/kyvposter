package nl.lakedigital.djfc.domain.virtueleoldtimerdag;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.File;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

public class Bestand {
    private File file;
    private List<KentekenMetWaardeMetHashtag> kentekenMetWaardeMetHashtags = newArrayList();

    public Bestand() {
    }

    public Bestand(File file, List<KentekenMetWaardeMetHashtag> kentekenMetWaardeMetHashtags) {
        this.file = file;
        this.kentekenMetWaardeMetHashtags = kentekenMetWaardeMetHashtags;
    }

    public Bestand(File file, List<String> kentekens, boolean dummy) {
        this.file = file;
        this.kentekenMetWaardeMetHashtags = kentekens.stream().map(new Function<String, KentekenMetWaardeMetHashtag>() {
            @Override
            public KentekenMetWaardeMetHashtag apply(String kenteken) {
                return new KentekenMetWaardeMetHashtag(kenteken);
            }
        }).collect(Collectors.toList());
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public List<KentekenMetWaardeMetHashtag> getKentekenMetWaardeMetHashtags() {
        return kentekenMetWaardeMetHashtags;
    }

    public void setKentekenMetWaardeMetHashtags(List<KentekenMetWaardeMetHashtag> kentekenMetWaardeMetHashtags) {
        this.kentekenMetWaardeMetHashtags = kentekenMetWaardeMetHashtags;
    }

    public String getTekst() {
        StringBuilder tekst = new StringBuilder();
        StringBuilder hashtags = new StringBuilder();

        kentekenMetWaardeMetHashtags.stream().forEach(new Consumer<KentekenMetWaardeMetHashtag>() {
            @Override
            public void accept(KentekenMetWaardeMetHashtag kentekenMetWaardeMetHashtag) {
                kentekenMetWaardeMetHashtag.getWaardeMetHashtags().stream().forEach(new Consumer<WaardeMetHashtag>() {
                    @Override
                    public void accept(WaardeMetHashtag waardeMetHashtag) {
                        tekst.append(waardeMetHashtag.getWaarde());
                        tekst.append(" ");

                        if (waardeMetHashtag.getHashtag() != null) {
                            hashtags.append("#");
                            hashtags.append(waardeMetHashtag.getHashtag());
                            hashtags.append(" ");
                        }
                    }
                });
            }
        });

        return tekst + "\n\n" + hashtags;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(null).append("file", file.getName()).append("kentekenMetWaardeMetHashtags", kentekenMetWaardeMetHashtags).toString();
    }
}
