package nl.heidotting.service;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@Service
public class FtpService {
    private final static Logger LOGGER = LoggerFactory.getLogger(FtpService.class);

    private String server;
    private int port;
    private String user;
    private String password;
    private FTPClient ftp;

    @PostConstruct
    public void init() {
        server = "185.231.202.119";
        port = 21;
        user = "cam@heidotting.nl";
        password = "sel18BrQ";

        ftp = new FTPClient();

        try {
            ftp.connect(server, port);
            ftp.enterLocalPassiveMode();
            int reply = ftp.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftp.disconnect();
                throw new IOException("Exception in connecting to FTP Server");
            }

            ftp.login(user, password);
        } catch (IOException e) {
            LOGGER.error("Fout ontstaan in {}", e);
        }
    }

    public void uploadBestand(String remoteNaam, byte[] input) {
        uploadBestand(remoteNaam, input, false);
    }

    public void uploadBestand(String remoteNaam, byte[] input, boolean recursive) {
        try {
            ftp.storeFile(remoteNaam, new ByteArrayInputStream(input));
        } catch (IOException e) {
            if (!recursive) {
                kill();
                init();
                uploadBestand(remoteNaam, input, true);
            } else {
                LOGGER.error("Fout ontstaan in {}", e);
            }
        }
    }

    @PreDestroy
    public void kill() {
        try {
            ftp.disconnect();
        } catch (IOException e) {
            LOGGER.error("Fout ontstaan in {}", e);
        }
    }

}
