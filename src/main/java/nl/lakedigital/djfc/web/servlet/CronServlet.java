package nl.lakedigital.djfc.web.servlet;

import nl.heidotting.dashboard.keeping.service.UrenNogTeGaanService;
import nl.heidotting.facebook.statistieken.service.FacebookStatistiekenService;
import nl.heidotting.service.FtpService;
import nl.heidotting.service.InstagramStatistiekenEnBotService;
import nl.heidotting.thuis.DownloadCameraImage;
import nl.heidotting.thuis.ResizeImageService;
import nl.heidotting.thuis.camera.CamService;
import nl.lakedigital.djfc.service.InlezenBestandenService;
import nl.lakedigital.djfc.service.PostInplannenEnUitvoerenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;

@Component
public class CronServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(CronServlet.class);

    @Inject
    private PostInplannenEnUitvoerenService postInplannenEnUitvoerenService;
    @Inject
    private InlezenBestandenService inlezenBestandenService;
    @Inject
    private UrenNogTeGaanService urenNogTeGaanService;
    @Inject
    private FacebookStatistiekenService facebookStatistiekenService;
    @Inject
    private InstagramStatistiekenEnBotService instagramStatistiekenEnBotService;
    @Inject
    private FtpService ftpService;
    @Inject
    private CamService camService;
    @Inject
    private ResizeImageService resizeImageService;


    @Scheduled(fixedDelay = 60000)
    public void urenNogTeGaan() {
        urenNogTeGaanService.bereken();
    }

    @Scheduled(fixedDelay = 7200000)
    public void facebookStatistieken() throws IOException {
        facebookStatistiekenService.ophalenGegevensVoorFacebook();
    }

    @Scheduled(fixedDelay = 28800000)
    public void instagramStatistieken() throws IOException {
        instagramStatistiekenEnBotService.trapAf();
    }

    @Scheduled(fixedDelay = 60000)
    public void downloadImage() throws Exception {
        (new DownloadCameraImage(ftpService, camService, resizeImageService)).start();
    }

    //    @Scheduled(fixedDelay = 174000)
    //    public void run() {
    //        LOGGER.info("Run");
    //        postInplannenEnUitvoerenService.planEnVoerUit();
    //    }

    //    @Scheduled(fixedDelay = 3600000)
    //    public void inlezenNieuweBestanden() {
    //        LOGGER.info("Inlezen bestanden");
    //
    //        try {
    //            inlezenBestandenService.inlezenBestanden();
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        }
    //    }
}
