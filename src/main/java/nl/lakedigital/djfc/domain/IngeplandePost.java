package nl.lakedigital.djfc.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "POSTS")
@NamedQueries({@NamedQuery(name = "IngeplandePost.ingeplandePostsVoorDatum", query = "select p from IngeplandePost p where p.tijdstipIngepland > :startTijdstip"),// and p.tijdstipUitgevoerd < :eindTijdstip"),//
        @NamedQuery(name = "IngeplandePost.ingeplandeOnverzondenPostsVoorDatum", query = "select p from IngeplandePost p where p.tijdstipUitgevoerd is null and p.tijdstipIngepland > :startTijdstip"),// and p.tijdstipIngepland < :eindTijdstip")
        @NamedQuery(name = "IngeplandePost.overgeblevenPosts", query = "select p from IngeplandePost p where p.tijdstipUitgevoerd is null"),// and p.tijdstipIngepland < :eindTijdstip")
        //        @NamedQuery(name = "IngeplandePost.leesBijResource", query = "select p from IngeplandePost p where p.resource = :resource"),//
        @NamedQuery(name = "IngeplandePost.laatstVerstuurdePost", query = "select p from IngeplandePost p order by p.tijdstipUitgevoerd desc"),//
        @NamedQuery(name = "IngeplandePost.opruimen", query = "delete from IngeplandePost p where p.tijdstipUitgevoerd < :tijdstip")})
public class IngeplandePost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "INGEPLAND")
    private LocalDateTime tijdstipIngepland;
    @Column(name = "UITGEVOERD")
    private LocalDateTime tijdstipUitgevoerd;
    @Column(name = "OPGEPAKT")
    private boolean opgepakt = false;
    @JoinColumn(name = "BESTAND")
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Bestand bestand;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getTijdstipIngepland() {
        return tijdstipIngepland;
    }

    public void setTijdstipIngepland(LocalDateTime tijdstipIngepland) {
        this.tijdstipIngepland = tijdstipIngepland;
    }

    public LocalDateTime getTijdstipUitgevoerd() {
        return tijdstipUitgevoerd;
    }

    public void setTijdstipUitgevoerd(LocalDateTime tijdstipUitgevoerd) {
        this.tijdstipUitgevoerd = tijdstipUitgevoerd;
    }

    public boolean isOpgepakt() {
        return opgepakt;
    }

    public void setOpgepakt(boolean opgepakt) {
        this.opgepakt = opgepakt;
    }

    public Bestand getBestand() {
        return bestand;
    }

    public void setBestand(Bestand bestand) {
        this.bestand = bestand;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("tijdstipIngepland", tijdstipIngepland).append("tijdstipUitgevoerd", tijdstipUitgevoerd).append("opgepakt", opgepakt).append("bestand", bestand).toString();
    }
}
