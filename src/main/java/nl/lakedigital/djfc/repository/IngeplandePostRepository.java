package nl.lakedigital.djfc.repository;

import nl.lakedigital.djfc.domain.IngeplandePost;
import org.hibernate.*;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Repository
public class IngeplandePostRepository {
    private final static Logger LOGGER = LoggerFactory.getLogger(IngeplandePostRepository.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Session getSession() {
        try {
            return sessionFactory.getCurrentSession();
        } catch (HibernateException e) {//NOSONAR
            return sessionFactory.openSession();
        }
    }

    protected Transaction getTransaction() {
        Transaction transaction = getSession().getTransaction();
        if (transaction.getStatus() != TransactionStatus.ACTIVE) {
            transaction.begin();
        }

        return transaction;
    }

    public void opslaan(IngeplandePost ingeplandePost) {
        LOGGER.info("Opslaan enkel");
        opslaan(newArrayList(ingeplandePost));
    }

    public void opslaan(List<IngeplandePost> ingeplandePosts) {
        LOGGER.info("Opslaan {} post(s)", ingeplandePosts.size());
        getTransaction();
        for (IngeplandePost ingeplandePost : ingeplandePosts) {
            if (ingeplandePost.getId() == null) {
                getSession().save(ingeplandePost);
            } else {
                getSession().merge(ingeplandePost);
            }
        }
        getTransaction().commit();
    }

    public void verwijder(IngeplandePost ingeplandePost) {
        getTransaction();
        getSession().delete(ingeplandePost);
        getTransaction().commit();
    }


    public IngeplandePost lees(Long id) {
        return getSession().get(IngeplandePost.class, id);
    }


    public List<IngeplandePost> leesBijResource(String resource) {
        Query query = getSession().getNamedQuery("IngeplandePost.leesBijResource");
        query.setParameter("resource", resource);

        return query.list();
    }


    public IngeplandePost laatstVerstuurdePost() {
        Query query = getSession().getNamedQuery("IngeplandePost.laatstVerstuurdePost").setMaxResults(1);

        return (IngeplandePost) query.uniqueResult();
    }


    public List<IngeplandePost> overgeblevenPosts() {
        Query query = getSession().getNamedQuery("IngeplandePost.overgeblevenPosts");

        return query.list();
    }


    public void opruimen() {
        LocalDateTime tijdstip = LocalDateTime.now().minusDays(2);

        Query query = getSession().getNamedQuery("IngeplandePost.opruimen");
        query.setParameter("tijdstip", tijdstip);

        query.executeUpdate();
    }


    public List<IngeplandePost> ingeplandePostsVoorDatum(LocalDate datum) {
        LocalDateTime startTijdstip = LocalDateTime.of(datum, LocalTime.of(0, 0, 1));
        LocalDateTime eindTijdstip = LocalDateTime.of(datum, LocalTime.of(23, 59, 59));

        Query query = getSession().getNamedQuery("IngeplandePost.ingeplandePostsVoorDatum");
        query.setParameter("startTijdstip", startTijdstip);
        //        query.setParameter("eindTijdstip", eindTijdstip);

        return query.list();
    }


    public List<IngeplandePost> ingeplandeOnverzondenPostsVoorDatum(LocalDate datum) {
        LocalDateTime startTijdstip = LocalDateTime.of(datum, LocalTime.of(0, 0, 1));
        LocalDateTime eindTijdstip = LocalDateTime.of(datum, LocalTime.of(23, 59, 59));

        Query query = getSession().getNamedQuery("IngeplandePost.ingeplandeOnverzondenPostsVoorDatum");
        query.setParameter("startTijdstip", startTijdstip);
        //        query.setParameter("eindTijdstip", eindTijdstip);

        return query.list();
    }

    public void refresh(IngeplandePost ingeplandePost) {
        getTransaction();
        getSession().refresh(ingeplandePost);
        getTransaction().commit();
    }
}
