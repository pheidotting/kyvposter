package nl.heidotting.dashboard.keeping.service;

import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.easymock.TestSubject;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(EasyMockRunner.class)
public class GemiddeldenServiceTest extends EasyMockSupport {
    @TestSubject
    private GemiddeldenService gemiddeldenService = new GemiddeldenService();

    @Test
    public void berekenUrenGemiddeldNogNodigGeenUrenGemaakt() {
        Map<String, Object> input = new HashMap<>();

        input.put("aantalUren1", 0.0);
        input.put("aantalUren2", 0.0);
        input.put("aantalUren3", 0.0);
        input.put("aantalUren4", 0.0);
        input.put("aantalUren5", 0.0);
        input.put("aantalUren6", 0.0);
        input.put("aantalUren7", 0.0);
        input.put("aantalUren", 0.0);

        LocalDate vandaag = LocalDate.of(2020, 5, 4);//Maandag

        assertThat(gemiddeldenService.berekenUrenGemiddeldNogNodig(input, vandaag), is(7.2));
    }

    @Test
    public void berekenUrenGemiddeldNogNodigUrenGemaaktMinderDanGemiddelde() {
        Map<String, Object> input = new HashMap<>();

        input.put("aantalUren1", 7.0);
        input.put("aantalUren2", 0.0);
        input.put("aantalUren3", 0.0);
        input.put("aantalUren4", 0.0);
        input.put("aantalUren5", 0.0);
        input.put("aantalUren6", 0.0);
        input.put("aantalUren7", 0.0);
        input.put("aantalUren", 7.0);

        LocalDate vandaag = LocalDate.of(2020, 5, 4);//Maandag

        assertThat(gemiddeldenService.berekenUrenGemiddeldNogNodig(input, vandaag), is(7.25));
    }

    @Test
    public void berekenUrenGemiddeldNogNodigUrenGemaaktGemiddelde() {
        Map<String, Object> input = new HashMap<>();

        input.put("aantalUren1", 7.2);
        input.put("aantalUren2", 0.0);
        input.put("aantalUren3", 0.0);
        input.put("aantalUren4", 0.0);
        input.put("aantalUren5", 0.0);
        input.put("aantalUren6", 0.0);
        input.put("aantalUren7", 0.0);
        input.put("aantalUren", 7.2);

        LocalDate vandaag = LocalDate.of(2020, 5, 4);//Maandag

        assertThat(gemiddeldenService.berekenUrenGemiddeldNogNodig(input, vandaag), is(7.2));
    }

    @Test
    public void berekenUrenGemiddeldNogNodigUrenGemaaktMeerDanGemiddelde() {
        Map<String, Object> input = new HashMap<>();

        input.put("aantalUren1", 8.0);
        input.put("aantalUren2", 0.0);
        input.put("aantalUren3", 0.0);
        input.put("aantalUren4", 0.0);
        input.put("aantalUren5", 0.0);
        input.put("aantalUren6", 0.0);
        input.put("aantalUren7", 0.0);
        input.put("aantalUren", 8.0);

        LocalDate vandaag = LocalDate.of(2020, 5, 4);//Maandag

        assertThat(gemiddeldenService.berekenUrenGemiddeldNogNodig(input, vandaag), is(7.0));
    }

    @Test
    public void berekenUrenGemiddeldNogNodigUrenGemaaktMeerDanGemiddelde2() {
        Map<String, Object> input = new HashMap<>();

        input.put("aantalUren1", 9.0);
        input.put("aantalUren2", 0.0);
        input.put("aantalUren3", 0.0);
        input.put("aantalUren4", 0.0);
        input.put("aantalUren5", 0.0);
        input.put("aantalUren6", 0.0);
        input.put("aantalUren7", 0.0);
        input.put("aantalUren", 8.0);

        LocalDate vandaag = LocalDate.of(2020, 5, 4);//Maandag

        assertThat(gemiddeldenService.berekenUrenGemiddeldNogNodig(input, vandaag), is(6.75));
    }

    @Test
    public void berekenUrenGemiddeldNogNodigUrenGemaaktHeleWeek() {
        Map<String, Object> input = new HashMap<>();

        input.put("aantalUren1", 9.0);
        input.put("aantalUren2", 9.0);
        input.put("aantalUren3", 7.0);
        input.put("aantalUren4", 9.0);
        input.put("aantalUren5", 0.0);
        input.put("aantalUren6", 0.0);
        input.put("aantalUren7", 0.0);
        input.put("aantalUren", 1.0);

        LocalDate vandaag = LocalDate.of(2020, 5, 8);//Vrijdag

        assertThat(gemiddeldenService.berekenUrenGemiddeldNogNodig(input, vandaag), is(1.0));
    }
}