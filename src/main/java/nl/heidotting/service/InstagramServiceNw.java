package nl.heidotting.service;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.common.util.concurrent.RateLimiter;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.brunocvcunha.instagram4j.Instagram4j;
import org.brunocvcunha.instagram4j.requests.*;
import org.brunocvcunha.instagram4j.requests.payload.*;
import org.brunocvcunha.inutils4j.MyNumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.lang.Thread.sleep;

@Service
public class InstagramServiceNw {
    private final static Logger LOGGER = LoggerFactory.getLogger(InstagramServiceNw.class);
    private Instagram4j instagram;
    private RateLimiter rateLimiter;

    private String username = "klveneryoungtimervrienden";
    private String password = "Klazienaveen2020!";
    public Long userid = 4917904352L;

    private final String timeOutMessage = "Please wait a few minutes before you try again.";

    private List<InstagramUserSummary> instagramFollowing;
    private List<InstagramUserSummary> instagramFollowers;
    private List<InstagramFeedItem> feed = new ArrayList<>();
    private Map<String, List<InstagramFeedItem>> tagsMetFeedItem = newHashMap();

    @PostConstruct
    public void init() throws IOException {
        if (rateLimiter == null) {
            rateLimiter = RateLimiter.create(1);
        }

        if (instagram == null) {
            instagram = Instagram4j.builder().username(username).password(password).build();
        }
        instagram.setup();
        instagram.login();
    }

    private Instagram4j getInstagram() {
        if (instagram == null || !instagram.isLoggedIn()) {
            try {
                init();
            } catch (IOException e) {
                LOGGER.error("Fout in getInstagram {}", e.getMessage(), e);
            }
        }
        return instagram;
    }

    public void loguit() {
        instagram = null;
    }

    /**
     * @return De Volgers van het klveneryoungtimervrienden account.
     */
    public List<InstagramUserSummary> getInstagramFollowers() {
        if (instagramFollowers == null) {
            LOGGER.info("Ophalen Followers");
            InstagramGetUserFollowersRequest instagramGetUserFollowersRequest = new InstagramGetUserFollowersRequest(userid);
            try {
                InstagramGetUserFollowersResult instagramGetUserFollowersResult = sendRequest(instagramGetUserFollowersRequest);
                if (timeOutMessage.equalsIgnoreCase(instagramGetUserFollowersResult.getMessage())) {
                    sleep(300000);
                    instagramGetUserFollowersResult = sendRequest(instagramGetUserFollowersRequest);
                }
                LOGGER.debug("Followers {}", instagramGetUserFollowersResult.getUsers());
                instagramFollowers = newArrayList(instagramGetUserFollowersResult.getUsers() == null ? newArrayList() : instagramGetUserFollowersResult.getUsers());

                LOGGER.debug("instagramGetUserFollowersResult.getNext_max_id() {}", instagramGetUserFollowersResult.getNext_max_id());
                while (instagramGetUserFollowersResult.getNext_max_id() != null) {
                    try {
                        instagramGetUserFollowersResult = sendRequest(new InstagramGetUserFollowersRequest(userid, instagramGetUserFollowersResult.getNext_max_id()));
                        List<InstagramUserSummary> lijst = instagramGetUserFollowersResult.getUsers();
                        if (lijst != null) {
                            instagramFollowers.addAll(lijst);
                        }
                        LOGGER.debug("Meer volgers ophalen {}", instagramGetUserFollowersResult.getNext_max_id());
                    } catch (NullPointerException npe) {
                        LOGGER.error("NPE opgetreden in getInstagramFollowers {} - {}", npe.getMessage(), npe);
                    }
                }
            } catch (IOException e) {
                LOGGER.error("Fout in getInstagramFollowers {}", e.getMessage(), e);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        LOGGER.debug("Opgehaald {} followers", instagramFollowers.size());
        return instagramFollowers;
    }

    /**
     * @return De Gevolgden van het klveneryoungtimervrienden account.
     */
    public List<InstagramUserSummary> getInstagramFollowing() {
        if (instagramFollowing == null) {
            LOGGER.info("Ophalen Following");
            InstagramGetUserFollowingRequest instagramGetUserFollowingRequest = new InstagramGetUserFollowingRequest(userid);
            try {
                InstagramGetUserFollowersResult instagramGetUserFollingResult = sendRequest(instagramGetUserFollowingRequest);
                if (timeOutMessage.equalsIgnoreCase(instagramGetUserFollingResult.getMessage())) {
                    sleep(300000);
                    instagramGetUserFollingResult = sendRequest(instagramGetUserFollowingRequest);
                }
                instagramFollowing = newArrayList(instagramGetUserFollingResult.getUsers() == null ? newArrayList() : instagramGetUserFollingResult.getUsers());

                LOGGER.debug("instagramGetUserFollingResult.getNext_max_id() {}", instagramGetUserFollingResult.getNext_max_id());
                while (instagramGetUserFollingResult.getNext_max_id() != null) {
                    instagramGetUserFollingResult = sendRequest(new InstagramGetUserFollowingRequest(userid, instagramGetUserFollingResult.getNext_max_id()));
                    instagramFollowing.addAll(instagramGetUserFollingResult.getUsers());
                    LOGGER.debug("Meer volgers ophalen {}", instagramGetUserFollingResult.getNext_max_id());
                }
            } catch (IOException e) {
                LOGGER.error("Fout in getInstagramFollowing {}", e.getMessage(), e);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        LOGGER.debug("Opgehaald {} following", instagramFollowing.size());
        return instagramFollowing;
    }

    public void reset() {
        instagramFollowers = null;
        instagramFollowing = null;
        feed = new ArrayList<>();
        tagsMetFeedItem = newHashMap();
    }

    public void resetFollowersEnFollowing() {
        instagramFollowers = null;
        instagramFollowing = null;
    }

    public Stream<InstagramFeedItem> zoekOpTag(String tag, Predicate predicate, Consumer consumer) {
        LocalDateTime eenJaarGeleden = LocalDateTime.now().minusYears(1);

        Supplier<Stream<InstagramFeedItem>> result = () -> {
            if (tagsMetFeedItem.get(tag) != null) {
                return tagsMetFeedItem.get(tag).stream();
            } else {
                try {
                    InstagramFeedResult instagramFeedResult = sendRequest(new InstagramTagFeedRequest(tag));
                    List<InstagramFeedItem> feedItems = newArrayList(instagramFeedResult.getItems());

                    while (instagramFeedResult.isMore_available()) {
                        LOGGER.debug("Meer ophalen {}", instagramFeedResult.getNext_max_id());
                        instagramFeedResult = sendRequest(new InstagramTagFeedRequest(tag, instagramFeedResult.getNext_max_id()));

                        boolean oudeFotoAanwezig = instagramFeedResult.getItems().stream().anyMatch(instagramFeedItem -> {
                            LocalDateTime tijdstip = Instant.ofEpochMilli(instagramFeedItem.getDevice_timestamp()).atZone(ZoneId.systemDefault()).toLocalDateTime();

                            //                            LOGGER.debug("{} - {} - {}",eenJaarGeleden.toString(),tijdstip.toString(),tijdstip.isBefore(eenJaarGeleden));
                            return tijdstip.isBefore(eenJaarGeleden);
                        });

                        feedItems.addAll(instagramFeedResult.getItems());

                        if (oudeFotoAanwezig) {
                            break;
                        }
                    }

                    tagsMetFeedItem.put(tag, feedItems);
                    return feedItems.stream();
                } catch (JsonMappingException e) {
                    LOGGER.error("Fout in zoekOpTag {}", e.getMessage(), e);
                    List<InstagramFeedItem> items = new ArrayList<>();
                    return items.stream();
                } catch (IOException e) {
                    LOGGER.error("Fout in zoekOpTag {}", e.getMessage(), e);
                }
            }
            return null;
        };
        if (predicate != null) {
            result.get().filter(predicate);
        }
        if (consumer != null) {
            result.get().forEach(consumer);
        }
        return result.get();
    }

    public void volgUserIndienNietAlGevolgd(InstagramUser userTeVolgen, boolean reset) {
        volgUserIndienNietAlGevolgd(instagramUserSummary(userTeVolgen), reset);
    }

    public void volgUserIndienNietAlGevolgd(InstagramUserSummary userTeVolgen, boolean reset) {
        if (userTeVolgen != null) {
            if (getInstagramFollowing().stream().noneMatch(instagramUserSummary -> instagramUserSummary != null && instagramUserSummary.getPk() == userTeVolgen.getPk()) && userTeVolgen.getPk() != userid) {
                try {
                    //Friendship status ophalen
                    InstagramFriendshipStatus instagramGetFriendshipResult = sendRequest(new InstagramGetFriendshipRequest(userTeVolgen.getPk()));

                    if (!instagramGetFriendshipResult.isOutgoing_request()) {
                        LOGGER.debug("User met pk {} en full name {} volgen", userTeVolgen.getPk(), userTeVolgen.getFull_name());

                        sendRequest(new InstagramFollowRequest(userTeVolgen.getPk()));
                        instagramFollowing.add(userTeVolgen);
                    }
                    if (reset) {
                        instagramFollowing = null;
                    }
                } catch (IOException e) {
                    LOGGER.error("Fout in volgUserIndienNietAlGevolgd {}", e.getMessage(), e);
                }
            }
        }
    }

    public boolean like(InstagramFeedItem teLiken) {
        boolean result = false;
        if (teLiken.getUser().getPk() != userid) {
            if (!teLiken.isHas_liked()) {
                try {
                    LOGGER.debug("Post liken {}", teLiken.getPk());
                    InstagramLikeResult instagramLikeResult = sendRequest(new InstagramLikeRequest(teLiken.getPk()));
                    LOGGER.debug("{}", ReflectionToStringBuilder.toString(instagramLikeResult));
                    LOGGER.debug("{}", instagramLikeResult.getFeedback_action());
                    LOGGER.debug("{}", instagramLikeResult.getFeedback_appeal_label());
                    LOGGER.debug("{}", instagramLikeResult.getFeedback_ignore_label());
                    LOGGER.debug("{}", instagramLikeResult.getFeedback_message());
                    LOGGER.debug("{}", instagramLikeResult.getFeedback_title());
                    LOGGER.debug("{}", instagramLikeResult.getFeedback_url());

                    return "ok".equalsIgnoreCase(instagramLikeResult.getStatus());
                } catch (IOException e) {
                    LOGGER.error("Fout in like {}", e.getMessage(), e);
                }
            }
        }

        return result;
    }

    public void unlike(InstagramFeedItem teUnLiken) {
        try {
            LOGGER.debug("Post unliken");
            sendRequest(new InstagramUnlikeRequest(teUnLiken.getPk()));
        } catch (IOException e) {
            LOGGER.error("Fout in like {}", e.getMessage(), e);
        }
    }

    public List<InstagramFeedItem> getFeed() {
        if (feed == null || feed.isEmpty()) {
            try {
                InstagramFeedResult instagramFeedResult = sendRequest(new InstagramUserFeedRequest(userid));
                List<InstagramFeedItem> result = instagramFeedResult.getItems();

                while (instagramFeedResult.isMore_available()) {
                    LOGGER.debug("Meer ophalen {}", instagramFeedResult.getNext_max_id());
                    instagramFeedResult = sendRequest(new InstagramUserFeedRequest(userid, instagramFeedResult.getNext_max_id(), LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli(), System.currentTimeMillis()));
                    result.addAll(instagramFeedResult.getItems());
                }
                LOGGER.info("{} items", result.size());
                feed = result;
            } catch (IOException e) {
                LOGGER.error("Fout in getFeed {}", e.getMessage(), e);
                return null;
            }
        }
        return feed;
    }

    public Stream getComments(long pk) {
        try {
            InstagramGetMediaCommentsResult commentsResult = sendRequest(new InstagramGetMediaCommentsRequest(String.valueOf(pk), "99"));
            return commentsResult.getComments().stream();
        } catch (IOException e) {
            LOGGER.error("Fout in getComments {}", e.getMessage(), e);
        }
        return null;
    }

    public boolean isEigen(InstagramFeedItem instagramFeedItem) {
        return instagramFeedItem.getUser().getPk() == userid;
    }

    public boolean isAlGeliked(InstagramFeedItem instagramFeedItem) {
        return instagramFeedItem.isHas_liked();
    }

    private <T> T sendRequest(InstagramRequest<T> request) throws IOException {
        rateLimiter.acquire(MyNumberUtils.randomIntBetween(10, 20));
        return getInstagram().sendRequest(request);
    }

    public void sendDirectMessage(String tekst, String linkTekst, String recipient) {
        InstagramDirectShareRequest.InstagramDirectShareRequestBuilder builder = InstagramDirectShareRequest.builder().//
                shareType(InstagramDirectShareRequest.ShareType.MESSAGE).//
                recipients(newArrayList(recipient));
        if (tekst != null) {
            builder.message(tekst);
        }
        if (linkTekst != null) {
            builder.link(linkTekst);
        }

        try {
            sendRequest(builder.build());
        } catch (IOException e) {
            LOGGER.error("Fout in sendDirectMessage {}", e.getMessage(), e);
        }
    }

    private InstagramUserSummary instagramUserSummary(InstagramUser instagramUser) {
        InstagramUserSummary instagramUserSummary = new InstagramUserSummary();

        instagramUserSummary.set_verified(instagramUser.is_verified());
        instagramUserSummary.setProfile_pic_id(instagramUser.getProfile_pic_id());
        instagramUserSummary.set_favorite(instagramUser.is_favorite());
        instagramUserSummary.set_private(instagramUser.is_private());
        instagramUserSummary.setUsername(instagramUser.getUsername());
        instagramUserSummary.setPk(instagramUser.getPk());
        instagramUserSummary.setProfile_pic_url(instagramUser.getProfile_pic_url());
        instagramUserSummary.setHas_anonymous_profile_picture(instagramUser.isHas_anonymous_profile_picture());
        instagramUserSummary.setFull_name(instagramUser.getFull_name());

        return instagramUserSummary;
    }
}
