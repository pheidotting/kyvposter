package nl.heidotting.thuis;

import nl.heidotting.service.FtpService;
import nl.heidotting.thuis.camera.CamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DownloadCameraImage extends Thread {
    private final static Logger LOGGER = LoggerFactory.getLogger(DownloadCameraImage.class);

    private FtpService ftpService;
    private CamService camService;
    private ResizeImageService resizeImageService;

    public DownloadCameraImage(FtpService ftpService, CamService camService, ResizeImageService resizeImageService) {
        this.ftpService = ftpService;
        this.camService = camService;
        this.resizeImageService = resizeImageService;
    }

    @Override
    public void run() {
        camService.getUrls().stream()//
                .map(camera -> camera.setImage(camService.downloadImage(camera)))//
                .map(camera -> camera.setImageKlein(resizeImageService.resize(camera.getImage(), 480, 270)))//
                .forEach(camera -> {
                    ftpService.uploadBestand("Camera-" + camera.getId() + "-Groot.jpg", camera.getImage());
                    ftpService.uploadBestand("Camera-" + camera.getId() + "-Klein.jpg", camera.getImageKlein());
                });
    }
}
