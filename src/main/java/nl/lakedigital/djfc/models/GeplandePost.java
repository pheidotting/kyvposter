//package nl.lakedigital.djfc.models;
//
//import nl.lakedigital.djfc.domain.Bestand;
//
//import java.time.LocalDateTime;
//
//public class GeplandePost {
//    public enum Media {
//        FACEBOOK, INSTAGRAM;
//    }
//
//    private Long id;
//    private Bestand bestand;
//    private LocalDateTime tijdstip;
//
//    public GeplandePost(Long id, Bestand bestand, LocalDateTime tijdstip) {
//        this.id = id;
//        this.bestand = bestand;
//        this.tijdstip = tijdstip;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public LocalDateTime getTijdstip() {
//        return tijdstip;
//    }
//
//    public Bestand getBestand() {
//        return bestand;
//    }
//}
