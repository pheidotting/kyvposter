package nl.heidotting.dashboard.keeping.service;

import io.prometheus.client.Gauge;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
public class MetricsService {
    private Map<String, Gauge> gauges = new HashMap<>();


    //    @Inject
    //    private MetricRegistry metricRegistry;
    //    private String graphiteUrl;
    //    CollectorRegistry registry ;
    //    private Graphite graphite;

    @PostConstruct
    public MetricsService init() {
        //        registry= new CollectorRegistry();
        //        if (graphiteUrl != null && !"".equals(graphiteUrl)) {
        //        metricRegistry = new MetricRegistry();
        //            graphite = new Graphite(new InetSocketAddress(graphiteUrl, 2003));
        //            GraphiteReporter reporter = GraphiteReporter.forRegistry(metricRegistry)//
        //                    .convertRatesTo(TimeUnit.SECONDS)//
        //                    .convertDurationsTo(TimeUnit.MILLISECONDS)//
        //                    .filter(MetricFilter.ALL).build(graphite);
        //            reporter.start(5, TimeUnit.MINUTES);
        //        }

        return this;
    }

    //    public void addMetric(String soortMetric, Class clazz) {
    //        //        if (graphite == null) {
    //        //            init();
    //        //        }
    //        metricRegistry.meter("meter." + name(clazz, soortMetric)).mark();
    //    }
    //
    //    public void addMetric(String soortMetric) {
    //        //        if (graphite == null) {
    //        //            init();
    //        //        }
    //        metricRegistry.meter(soortMetric).mark();
    //    }
    //
    //    public Timer.Context addTimerMetric(String naam, Class c) {
    //        //        if (graphite == null) {
    //        //            init();
    //        //        }
    //        final Timer timer1 = metricRegistry.timer("timer." + name(c, naam));
    //        return timer1.time();
    //    }
    //
    //    public Counter getCounter(String counterNaam) {
    //        //        if (graphite == null) {
    //        //            init();
    //        //        }
    //        return metricRegistry.counter(counterNaam);
    //    }
    //
    //        public Counter getCounter(String counterNaam, Class c) {
    //            return getCounter(name(c, counterNaam));
    //        }
    //
    //    public Gauge getGauge(String naam, Class c, final MetricRegistry.MetricSupplier<Gauge> supplier) {
    //        return metricRegistry.gauge(naam, supplier);
    //    }
    //
    //    public void stop(Timer.Context timer) {
    //        timer.stop();
    //    }
    //
    //    public MetricRegistry getMetricRegistry() {
    //        return metricRegistry;
    //    }
    public Gauge getGauge(Class c, String naam, String... labels) {
        Gauge g = gauges.get(naam);
        if (g == null) {
            g = Gauge.build().name(name(c, naam)).help(naam).labelNames(labels).register();
            gauges.put(naam, g);
        }

        return g;
    }

    public Gauge getGauge(Class c, String naam) {
        Gauge g = gauges.get(naam);
        if (g == null) {
            g = Gauge.build().name(name(c, naam)).help(naam).register();
            gauges.put(naam, g);
        }

        return g;
    }

    private static String name(Class<?> klass, String... names) {
        return name(klass.getName(), names);
    }

    private static String name(String name, String... names) {
        StringBuilder builder = new StringBuilder();

        String[] parts = name.split("\\.");
        for (String p : parts) {
            append(builder, p);
        }
        if (names != null) {
            String[] var3 = names;
            int var4 = names.length;

            for (int var5 = 0; var5 < var4; ++var5) {
                String s = var3[var5];
                append(builder, s);
            }
        }

        return builder.toString();
    }

    private static void append(StringBuilder builder, String part) {
        if (part != null && !part.isEmpty()) {
            if (builder.length() > 0) {
                builder.append('_');
            }

            builder.append(part);//part.substring(0, 1).toUpperCase() + part.substring(1));
        }

    }


}
