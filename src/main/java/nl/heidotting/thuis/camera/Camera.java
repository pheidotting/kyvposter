package nl.heidotting.thuis.camera;

public class Camera {
    private int id;
    private String url;
    private byte[] image;
    private byte[] imageKlein;

    public Camera(int id, String url) {
        this.id = id;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public Camera setId(int id) {
        this.id = id;

        return this;
    }

    public String getUrl() {
        return url;
    }

    public Camera setUrl(String url) {
        this.url = url;

        return this;
    }

    public byte[] getImage() {
        return image;
    }

    public Camera setImage(byte[] image) {
        this.image = image;

        return this;
    }

    public byte[] getImageKlein() {
        return imageKlein;
    }

    public Camera setImageKlein(byte[] imageKlein) {
        this.imageKlein = imageKlein;

        return this;
    }
}
