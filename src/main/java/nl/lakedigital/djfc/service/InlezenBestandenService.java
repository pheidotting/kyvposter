package nl.lakedigital.djfc.service;

import com.github.sardine.DavResource;
import nl.lakedigital.djfc.domain.Bestand;
import nl.lakedigital.djfc.domain.Media;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@Service
public class InlezenBestandenService {
    @Inject
    private StackStorageService stackStorageService;
    @Inject
    private BestandService bestandService;

    public void inlezenBestanden() throws IOException {
        List<DavResource> resources = stackStorageService.haalLijstOpServerOp(stackStorageService.getWEBDAV_SERVER() + stackStorageService.getWEBDAV_PATH_NIEUWE_BESTANDEN());

        //eerst de bestanden inlezen
        resources.stream().forEach(davResource -> {
            System.out.println(davResource.toString());
            Bestand bestandFb = new Bestand(Media.FACEBOOK, davResource.getPath().replace(stackStorageService.getWEBDAV_PATH_NIEUWE_BESTANDEN(), ""));
            stackStorageService.copy(davResource, stackStorageService.getWEBDAV_PATH() + "/" + bestandFb.getIdentificatie());

            Bestand bestandIg = new Bestand(Media.INSTAGRAM, davResource.getPath().replace(stackStorageService.getWEBDAV_PATH_NIEUWE_BESTANDEN(), ""));
            stackStorageService.move(davResource, stackStorageService.getWEBDAV_PATH() + "/" + bestandIg.getIdentificatie());

            bestandService.opslaan(newArrayList(bestandFb, bestandIg));
        });

        stackStorageService.lijstMetBestandenEnMappen(stackStorageService.getWEBDAV_SERVER() + stackStorageService.getWEBDAV_PATH_NIEUWE_BESTANDEN()).stream().forEach(davResource -> {
            if (!davResource.toString().equals(stackStorageService.getWEBDAV_PATH_NIEUWE_BESTANDEN() + "/")) {
                stackStorageService.verwijder(davResource);
            }
        });
    }
}