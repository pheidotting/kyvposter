package nl.heidotting.facebook.statistieken.service;

import io.prometheus.client.Gauge;
import nl.heidotting.dashboard.keeping.service.MetricsService;
import nl.lakedigital.djfc.service.FacebookService;
import org.apache.commons.lang3.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

import static com.google.common.collect.Maps.newHashMap;
import static java.util.stream.Collectors.toList;

@Service
public class FacebookStatistiekenService {
    private final static Logger LOGGER = LoggerFactory.getLogger(FacebookStatistiekenService.class);

    private final String pageId = "534731113372521";

    @Inject
    private MetricsService metricsService;

    private Facebook facebook;
    private Gauge fanCountGauge;
    private Gauge albumCountGauge;
    private Gauge fotosCountGauge;
    private Gauge attendingCountGauge;
    private Gauge maybeCountGauge;

    public void init() throws IOException {
        facebook = new FacebookTemplate(FacebookService.accessCode);
        PageMetFancount p = facebook.fetchObject(pageId, PageMetFancount.class, "name", "fan_count");

        String naamCamelCase = p.getName().substring(0, 1).toLowerCase() + WordUtils.capitalizeFully(p.getName()).replace(" ", "").substring(1);
        fanCountGauge = metricsService.getGauge(this.getClass(), naamCamelCase + "FanCountGauge");
        albumCountGauge = metricsService.getGauge(this.getClass(), naamCamelCase + "AlbumCountGauge");
        fotosCountGauge = metricsService.getGauge(this.getClass(), naamCamelCase + "FotosCountGauge");
        attendingCountGauge = metricsService.getGauge(this.getClass(), naamCamelCase + "AttendingCountGauge", "name");
        maybeCountGauge = metricsService.getGauge(this.getClass(), naamCamelCase + "MaybeCountGauge");

        ophalenGegevensVoorFacebook();
    }

    public void ophalenGegevensVoorFacebook() throws IOException {
        if (facebook == null) {
            init();
        }
        PageMetFancount p = facebook.fetchObject(pageId, PageMetFancount.class, "name", "fan_count");
        fanCountGauge.set(p.getFan_count());

        Supplier<Stream> albumStream = getAlbums();
        int fotos = albumStream.get().mapToInt((ToIntFunction<AlbumMetNameEnCount>) value -> value.getCount()).sum();

        albumCountGauge.set(((List) albumStream.get().collect(toList())).size());
        fotosCountGauge.set(fotos);

        Optional<Event> eventOptional = getEvents().get().findFirst();
        if (eventOptional.isPresent()) {
            Event event = eventOptional.get();
            attendingCountGauge.labels(event.getName()).set(event.getAttending_count());
            maybeCountGauge.set(event.getMaybe_count());
        }

        //534731113372521?fields=events{attending_count,declined_count,maybe_count,start_time}

        //        albums.stream().forEach(new Consumer<Album>() {
        //            @Override
        //            public void accept(Album album) {
        //                System.out.println("# # # # # # # # # # # # # # # ");
        //                System.out.println(" # # # # # # # # # # # # # # # ");
        //                System.out.println("# # # # # # # # # # # # # # # ");
        //                System.out.println(" # # # # # # # # # # # # # # # ");
        //                System.out.println(album.getName() + " - " + album.getCount());
        //
        //                List<Photo> photos = newArrayList();
        //                PagedList<Photo> photosPaged = mediaOperations.getPhotos(album.getId());
        //                photosPaged.stream().forEach(photo -> photos.add(photo));
        //
        //                while (photosPaged.getNextPage() != null) {
        //                    photosPaged = mediaOperations.getPhotos(pageId, photosPaged.getNextPage());
        //                    photosPaged.stream().forEach(photo -> photos.add(photo));
        //                }
        //
        //                photos.stream().forEach(new Consumer<Photo>() {
        //                    @Override
        //                    public void accept(Photo photo) {
        //                        System.out.println(photo.getName() + " - " + photo.getId());
        //                    }
        //                });
        //            }
        //        });

        //        System.out.println("# # # # # # # # # # # # # # # ");
        //        System.out.println(" # # # # # # # # # # # # # # # ");
        //        System.out.println("# # # # # # # # # # # # # # # ");
        //        System.out.println(" # # # # # # # # # # # # # # # ");
        //        albums.stream().filter(new Predicate<Album>() {
        //            @Override
        //            public boolean test(Album album) {
        //                return !"Tijdlijnfoto's".equalsIgnoreCase(album.getName())&&!"Uploads vanaf je telefoon".equalsIgnoreCase(album.getName());
        //            }
        //        }).forEach(new Consumer<Album>() {
        //            @Override
        //            public void accept(Album album) {
        //                System.out.println(album.getId()+" - "+album.getName()+" - "+album.getCount());
        //                likes(facebook.likeOperations(),album.getId());
        //            }
        //        });

    }

    private Supplier<Stream> getAlbums() {
        Map<String, AlbumMetNameEnCount> albums = newHashMap();
        PagedList<AlbumMetNameEnCount> albumsPaged = facebook.fetchConnections(pageId, "albums", AlbumMetNameEnCount.class, "name", "count");
        albumsPaged.stream().forEach(album -> albums.put(album.getId(), album));

        while (albumsPaged.getNextPage() != null) {
            albumsPaged = facebook.fetchConnections(pageId, "albums", AlbumMetNameEnCount.class, albumsPaged.getNextPage().toMap(), "name", "count");
            albumsPaged.stream().forEach(album -> albums.put(album.getId(), album));
        }

        return () -> albums.values().stream().filter(//
                album -> !album.getName().equalsIgnoreCase("Tijdlijnfoto's") && !album.getName().equalsIgnoreCase("Uploads vanaf je telefoon") && !album.getName().equalsIgnoreCase("Omslagfoto's"));
    }

    private Supplier<Stream> getEvents() {
        Map<String, Event> albums = newHashMap();
        PagedList<Event> eventPagedList = facebook.fetchConnections(pageId, "events", Event.class, "attending_count", "declined_count", "maybe_count", "start_time", "name");
        eventPagedList.stream().forEach(event -> albums.put(event.getId(), event));

        while (eventPagedList.getNextPage() != null) {
            eventPagedList = facebook.fetchConnections(pageId, "events", Event.class, eventPagedList.getNextPage().toMap(), "attending_count", "declined_count", "maybe_count", "start_time", "name");
            eventPagedList.stream().forEach(event -> albums.put(event.getId(), event));
        }

        return () -> albums.values().stream().sorted((o1, o2) -> o2.getStart_time().compareTo(o1.getStart_time()));
    }
}
