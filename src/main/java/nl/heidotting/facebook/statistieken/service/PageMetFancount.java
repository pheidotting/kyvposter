package nl.heidotting.facebook.statistieken.service;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PageMetFancount {
    @JsonProperty("fan_count")
    private int fan_count;
    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private String id;

    public PageMetFancount() {
    }

    public PageMetFancount(int fan_count, String name, String id) {
        this.fan_count = fan_count;
        this.name = name;
        this.id = id;
    }

    public int getFan_count() {
        return fan_count;
    }

    public void setFan_count(int fan_count) {
        this.fan_count = fan_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}