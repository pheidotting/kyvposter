package nl.heidotting.dashboard.keeping.service;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class UrenService {
    public List<LocalDate> getDagenDezeWeek() {
        LocalDate l = LocalDate.now();

        int dagNummer = l.getDayOfWeek().getValue();

        List<LocalDate> dagen = new ArrayList<>();

        while (dagNummer > 1) {
            l = l.minusDays(1);
            dagNummer--;
        }

        for (int i = 0; i < 7; i++) {
            dagen.add(l.plusDays(i));
        }

        return dagen;
    }
}
