package nl.heidotting.dashboard.keeping.service;

import io.prometheus.client.Gauge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Service
public class UrenNogTeGaanService {
    private final static Logger LOGGER = LoggerFactory.getLogger(UrenNogTeGaanService.class);

    @Inject
    private UrenService urenService;
    @Inject
    private MetricsService metricsService;
    @Inject
    private KeepingService keepingService;
    @Inject
    private GemiddeldenService gemiddeldenService;

    private Double uren;
    private Double urenNogTeGaan;
    private Double urenNogTeGaanNietNegatief;
    private Double ongoing;

    private Gauge urenMaandagGauge;
    private Gauge urenDinsdagGauge;
    private Gauge urenWoensdagGauge;
    private Gauge urenDonderdagGauge;
    private Gauge urenVrijdagGauge;
    private Gauge urenZaterdagGauge;
    private Gauge urenZondagGauge;

    private Gauge urenGauge;
    private Gauge urenNogTeGaanGauge;
    private Gauge urenNogTeGaanNietNegatiefGauge;
    private Gauge ongoingGauge;
    private Gauge gemiddeldeUrenPerdagGauge;
    private Gauge gemiddeldeUrenNodigPerdagGauge;

    @PostConstruct
    public void init() {
        urenMaandagGauge = metricsService.getGauge(this.getClass(), "urenMaandagGauge");
        urenDinsdagGauge = metricsService.getGauge(this.getClass(), "urenDinsdagGauge");
        urenWoensdagGauge = metricsService.getGauge(this.getClass(), "urenWoensdagGauge");
        urenDonderdagGauge = metricsService.getGauge(this.getClass(), "urenDonderdagGauge");
        urenVrijdagGauge = metricsService.getGauge(this.getClass(), "urenVrijdagGauge");
        urenZaterdagGauge = metricsService.getGauge(this.getClass(), "urenZaterdagGauge");
        urenZondagGauge = metricsService.getGauge(this.getClass(), "urenZondagGauge");

        urenGauge = metricsService.getGauge(this.getClass(), "urenGauge");
        urenNogTeGaanGauge = metricsService.getGauge(this.getClass(), "urenNogTeGaanGauge");
        urenNogTeGaanNietNegatiefGauge = metricsService.getGauge(this.getClass(), "urenNogTeGaanNietNegatiefGauge");
        ongoingGauge = metricsService.getGauge(this.getClass(), "ongoingGauge");
        gemiddeldeUrenPerdagGauge = metricsService.getGauge(this.getClass(), "gemiddeldeUrenPerdagGauge");
        gemiddeldeUrenNodigPerdagGauge = metricsService.getGauge(this.getClass(), "gemiddeldeUrenNodigPerdagGauge");

        bereken();
    }

    public void bereken() {
        List<LocalDate> dagen = urenService.getDagenDezeWeek();

        Map<String, Object> urenOpgehaald = keepingService.leesTotaalAantalUren(dagen);
        uren = (Double) urenOpgehaald.get("aantalUren");
        boolean ongoing = (boolean) urenOpgehaald.get("ongoing") == true;

        this.ongoing = ongoing == true ? 1.0 : 0.0;

        urenNogTeGaan = 36 - uren;

        urenNogTeGaanNietNegatief = urenNogTeGaan > 0.0 ? urenNogTeGaan : 0.0;
        try {
            urenMaandagGauge.set((Double) urenOpgehaald.get("aantalUren1"));
            urenDinsdagGauge.set((Double) urenOpgehaald.get("aantalUren2"));
            urenWoensdagGauge.set((Double) urenOpgehaald.get("aantalUren3"));
            urenDonderdagGauge.set((Double) urenOpgehaald.get("aantalUren4"));
            urenVrijdagGauge.set((Double) urenOpgehaald.get("aantalUren5"));
            urenZaterdagGauge.set((Double) urenOpgehaald.get("aantalUren6"));
            urenZondagGauge.set((Double) urenOpgehaald.get("aantalUren7"));
            urenGauge.set(uren);
            urenNogTeGaanGauge.set(urenNogTeGaan);
            urenNogTeGaanNietNegatiefGauge.set(urenNogTeGaanNietNegatief);
            ongoingGauge.set(this.ongoing);

            gemiddeldeUrenPerdagGauge.set(uren / LocalDate.now().getDayOfWeek().getValue());
            gemiddeldeUrenNodigPerdagGauge.set(gemiddeldenService.berekenUrenGemiddeldNogNodig(urenOpgehaald, LocalDate.now()));
        } catch (Exception e) {
            LOGGER.error("Somting wong");
        }
    }
}
