package nl.heidotting.facebook.statistieken.service;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlbumMetNameEnCount {
    @JsonProperty("count")
    private int count;
    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private String id;
    @JsonProperty("created_time")
    private String created_time;

    public AlbumMetNameEnCount() {
    }

    public AlbumMetNameEnCount(int count, String name, String id) {
        this.count = count;
        this.name = name;
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_time() {
        return created_time;
    }

    public void setCreated_time(String created_time) {
        this.created_time = created_time;
    }
}