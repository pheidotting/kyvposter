package nl.lakedigital.djfc.service;

import nl.lakedigital.djfc.domain.Bestand;
import nl.lakedigital.djfc.repository.BestandRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;

@Service
public class BestandService {
    @Inject
    private BestandRepository bestandRepository;

    public void opslaan(List<Bestand> bestanden) {
        bestandRepository.opslaan(bestanden);
    }

    public void opslaan(Bestand bestand) {
        bestandRepository.opslaan(bestand);
    }

    public List<Bestand> leesRandom(int aantal) {
        return bestandRepository.leesRandomAantal(aantal);
    }
}
