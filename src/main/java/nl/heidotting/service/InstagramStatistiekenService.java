package nl.heidotting.service;

import io.prometheus.client.Gauge;
import nl.heidotting.dashboard.keeping.service.MetricsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.atomic.AtomicInteger;

public class InstagramStatistiekenService extends Thread {
    private final static Logger LOGGER = LoggerFactory.getLogger(InstagramStatistiekenService.class);

    private MetricsService metricsService;
    private InstagramServiceNw instagramService;

    private Gauge instagramFollowersGauge;
    private Gauge instagramFollowingGauge;
    private Gauge instagramPostsGauge;
    private Gauge instagramLikesGauge;
    private Gauge instagramCommentsGauge;
    private boolean alleenVolgers = false;

    public InstagramStatistiekenService(MetricsService metricsService, InstagramServiceNw instagramService, Gauge instagramFollowersGauge, Gauge instagramFollowingGauge, Gauge instagramPostsGauge, Gauge instagramLikesGauge, Gauge instagramCommentsGauge, boolean alleenVolgers) {
        this.metricsService = metricsService;
        this.instagramService = instagramService;
        this.instagramFollowersGauge = instagramFollowersGauge;
        this.instagramFollowingGauge = instagramFollowingGauge;
        this.instagramPostsGauge = instagramPostsGauge;
        this.instagramLikesGauge = instagramLikesGauge;
        this.instagramCommentsGauge = instagramCommentsGauge;
        this.alleenVolgers = alleenVolgers;
    }

    @Override
    public void run() {
        ophalenGegevensVoorInstagram();
    }

    public void ophalenGegevensVoorInstagram() {
        LOGGER.info("Feedsize zetten");
        instagramPostsGauge.set(instagramService.getFeed().size());
        LOGGER.info("Aantal volgers zetten");
        instagramFollowersGauge.set(instagramService.getInstagramFollowers().size());
        LOGGER.info("Aantal volgend zetten");
        instagramFollowingGauge.set(instagramService.getInstagramFollowing().size());

        if (!alleenVolgers) {
            AtomicInteger totaalLikes = new AtomicInteger();
            totaalLikes.set(0);
            AtomicInteger totaalComments = new AtomicInteger();
            totaalComments.set(0);
            LOGGER.debug("Aantal likes en comments opzoeken");
            instagramService.getFeed().stream().filter(instagramFeedItem -> instagramFeedItem != null)//
                    .forEach(instagramFeedItem -> {
                        totaalLikes.set(totaalLikes.get() + instagramFeedItem.getLike_count());
                        instagramLikesGauge.set(totaalLikes.get());
                        totaalComments.set(totaalComments.get() + instagramFeedItem.getComment_count());
                        instagramCommentsGauge.set(totaalComments.get());
                    });
            instagramLikesGauge.set(totaalLikes.get());
            instagramCommentsGauge.set(totaalComments.get());
        }

        instagramService.loguit();
    }
}
