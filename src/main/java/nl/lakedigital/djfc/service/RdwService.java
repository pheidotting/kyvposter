package nl.lakedigital.djfc.service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import nl.lakedigital.djfc.domain.virtueleoldtimerdag.Bestand;
import nl.lakedigital.djfc.domain.virtueleoldtimerdag.KentekenMetWaardeMetHashtag;
import nl.lakedigital.djfc.domain.virtueleoldtimerdag.WaardeMetHashtag;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

@Service
public class RdwService {


    public Function leesHashTagsMetWaade() {
        return new Function<Bestand, Bestand>() {
            @Override
            public Bestand apply(Bestand bestand) {
                //        KentekenMetWaardeMetHashtag ret = new KentekenMetWaardeMetHashtag();

                bestand.getKentekenMetWaardeMetHashtags().stream().forEach(new Consumer<KentekenMetWaardeMetHashtag>() {
                    @Override
                    public void accept(KentekenMetWaardeMetHashtag kentekenMetWaardeMetHashtag) {
                        String kenteken = kentekenMetWaardeMetHashtag.getKenteken().replace("-", "").toUpperCase();
                        //        System.out.println(kenteken);

                        ClientConfig clientConfig = new DefaultClientConfig();
                        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
                        Client client = Client.create(clientConfig);
                        WebResource webResource = client.resource("https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=" + kenteken);
                        ClientResponse response = webResource.header("X-App-Token", "9HYD14uUG3kN5aobEr3phPHJe").accept("application/json").type("application/json").get(ClientResponse.class);

                        String res = response.getEntity(String.class);
                        //        System.out.println(res);

                        int posMerk = res.indexOf("\"merk\"");
                        if (posMerk > 1) {
                            int posMerkEnd = res.indexOf("\"", posMerk + 8);
                            String merk = res.substring(posMerk + 8, posMerkEnd);
                            int posHandelsbenaming = res.indexOf("\"handelsbenaming\"");
                            int posHandelsbenamingEnd = res.indexOf("\"", posHandelsbenaming + 19);
                            String handelsbenaming = res.substring(posHandelsbenaming + 19, posHandelsbenamingEnd);
                            int posDatumEersteToelating = res.indexOf("\"datum_eerste_toelating\"");
                            String bouwjaar = res.substring(posDatumEersteToelating + 26, posDatumEersteToelating + 30);

                            //        System.out.println(merk);
                            //        System.out.println(handelsbenaming);
                            //handelsbenaming
                            //merk

                            handelsbenaming = handelsbenaming.replace(" ", "").replace(";", "").replace(".", "");

                            //            ret.setKenteken(kenteken);
                            kentekenMetWaardeMetHashtag.getWaardeMetHashtags().add(new WaardeMetHashtag(merk, verwerkHashtag(merk)));
                            kentekenMetWaardeMetHashtag.getWaardeMetHashtags().add(new WaardeMetHashtag(handelsbenaming, verwerkHashtag(handelsbenaming)));
                            kentekenMetWaardeMetHashtag.getWaardeMetHashtags().add(new WaardeMetHashtag(bouwjaar, verwerkHashtag(bouwjaar)));
                        }
                    }
                });

                return bestand;
            }
        };
    }

    public List<String> leesHashTags(String kenteken) {
        List<String> ret = new ArrayList();

        kenteken = kenteken.replace("-", "").toUpperCase();
        //        System.out.println(kenteken);

        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        Client client = Client.create(clientConfig);
        WebResource webResource = client.resource("https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=" + kenteken);
        ClientResponse response = webResource.header("X-App-Token", "9HYD14uUG3kN5aobEr3phPHJe").accept("application/json").type("application/json").get(ClientResponse.class);

        String res = response.getEntity(String.class);
        System.out.println(res);

        int posMerk = res.indexOf("\"merk\"");
        if (posMerk > 1) {
            int posMerkEnd = res.indexOf("\"", posMerk + 8);
            String merk = res.substring(posMerk + 8, posMerkEnd);
            int posHandelsbenaming = res.indexOf("\"handelsbenaming\"");
            int posHandelsbenamingEnd = res.indexOf("\"", posHandelsbenaming + 19);
            String handelsbenaming = res.substring(posHandelsbenaming + 19, posHandelsbenamingEnd);
            int posDatumEersteToelating = res.indexOf("\"datum_eerste_toelating\"");
            System.out.println(posDatumEersteToelating);
            int posDatumEersteToelatingEnd = res.indexOf("\"", posHandelsbenaming + 26);
            System.out.println(posDatumEersteToelatingEnd);
            String bouwjaar = res.substring(posDatumEersteToelating + 26, posDatumEersteToelating + 30);
            System.out.println(bouwjaar);

            //        System.out.println(merk);
            //        System.out.println(handelsbenaming);
            //handelsbenaming
            //merk

            handelsbenaming = handelsbenaming.replace(" ", "").replace(";", "").replace(".", "");

            ret.add(verwerkHashtag(merk));
            ret.add(verwerkHashtag(handelsbenaming));
            ret.add(verwerkHashtag(bouwjaar));
        }

        return ret;
    }

    private String verwerkHashtag(String hash) {
        String hashtag = hash.replace(" ", "").replace("-", "").replace("*", "").replace(",", "").replace("/", "");

        try {
            Integer.parseInt(hashtag);

            return null;
        } catch (Exception e) {
            return hashtag.toLowerCase();
        }
    }
}
